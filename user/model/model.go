package model

type User struct {
	Id    string `json:"id,omitempty" bson:"_id,omitempty"`
	Name  string `json:"name,omitempty" bson:"name,omitempty"`
	Age   int    `json:"age,omitempty" bson:"age,omitempty"`
	Email string `json:"email,omitempty" bson:"email,omitempty"`
}

type GetList struct {
	Limit int `json:"limit"`
	Page  int `json:"page"`
}

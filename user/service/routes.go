package service

import (
	"testmongodriver/user/config"
	"testmongodriver/user/handlers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var cfg = config.GetConfig()

func Routes() *mux.Router {
	router := mux.NewRouter()
	s := router.PathPrefix("/api/v1/user").Subrouter()
	// /api/version/service/...
	s.HandleFunc("/get-user", handlers.GetUserById).Methods("POST")
	s.HandleFunc("/create", handlers.CreatedUser).Methods("POST")
	s.HandleFunc("/get-list-users", handlers.GetUsers).Methods("POST")

	return router
}

func Start() {

	router := Routes()

	log.Println("Running on Port:", cfg.Port)
	log.Fatal(http.ListenAndServe(":"+cfg.Port, router))
}

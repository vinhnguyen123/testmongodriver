package main

import (
	"testmongodriver/user/config"
	"testmongodriver/user/service"
)

var cfg = config.GetConfig()

func main() {

	cfg.MongoDriver.Get("btaskee").InitDriver()
	go func() {
		service.Start()
	}()
	// ABC345
	select {}
}

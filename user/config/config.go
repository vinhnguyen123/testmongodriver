package config

import (
	"flag"
	"fmt"
	"os"

	Viper "github.com/spf13/viper"
	"gitlab.com/btaskee/go-services-model/dbConfig"
)

type Config struct {
	Port        string `mapstructure:"running_rest_rating_service_port"`
	MongoDriver dbConfig.MongosDriver
}

var cfg *Config

func init() {
	var folder string
	mode := os.Getenv("APPLICATION_MODE")

	switch mode {
	case "dev", "prod", "test":
		folder = mode
	default:
		folder = "local"
	}

	var cfgPath string
	if flag.Lookup("cfgPath") == nil {
		flag.StringVar(&cfgPath, "cfgPath", "config", "")
	}
	flag.Parse()
	path := fmt.Sprintf("%v/%v", cfgPath, folder)

	cfg = new(Config)
	initConfig(path, "base", &cfg)
	initConfig(path, "mongo", &cfg.MongoDriver)
}

func initConfig(configPath, configName string, result interface{}) {
	viper := Viper.New()
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	err := viper.ReadInConfig() // Find and read the config file
	if err == nil {             // Handle errors reading the config file
		err = viper.Unmarshal(result)
		if err != nil { // Handle errors reading the config file
			panic(fmt.Errorf("Fatal error config file: %s", err))
		}
	}
}

func GetConfig() *Config {
	return cfg
}

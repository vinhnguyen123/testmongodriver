package handlers

import (
	"encoding/json"
	"net/http"
	"testmongodriver/user/model"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/btaskee/go-api-service/lib"
	"gitlab.com/btaskee/go-services-model/globalConstant"
	"gitlab.com/btaskee/go-services-model/globalLib"
	globalRepo "gitlab.com/btaskee/go-services-model/globalRepo"
	"go.mongodb.org/mongo-driver/bson"
)

func GetUserById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	user := map[string]interface{}{}
	var result *model.User
	_ = json.NewDecoder(r.Body).Decode(&user)

	globalRepo.GetOneById(globalConstant.COLLECTION_USERS, user["userId"].(string), nil, &result)
	json.NewEncoder(w).Encode(result)
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params model.GetList
	_ = json.NewDecoder(r.Body).Decode(&params)
	var result []*model.User
	page := 1
	limit := lib.PAGING_LIMIT
	if params.Page > 0 {
		page = params.Page
	}
	if params.Limit > 0 {
		limit = params.Limit
	}
	skip := (page - 1) * limit

	data, _ := globalRepo.GetAllByQuerySortPaging(globalConstant.COLLECTION_USERS, bson.M{}, nil, bson.M{"name": 1}, int64(skip), int64(limit))
	mapstructure.Decode(data, &result)

	json.NewEncoder(w).Encode(result)
}

func CreatedUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var user model.User
	_ = json.NewDecoder(r.Body).Decode(&user)

	if user.Name == "" {
		err := map[string]interface{}{
			"error": "Name is blank",
		}
		json.NewEncoder(w).Encode(err)

		return
	}
	user.Id = globalLib.GenerateObjectId()
	result, err := globalRepo.InsertOne(globalConstant.COLLECTION_USERS, user)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	userRe := map[string]interface{}{
		"id": result,
	}
	json.NewEncoder(w).Encode(userRe)
}
